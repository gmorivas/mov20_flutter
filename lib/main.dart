import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App Chida',
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      home: const ListaWidget(),
    );
  }
}

/*
Scaffold(
        appBar: AppBar(title: const Text('aplicacion muy chida')),
        body: const Center(child: Text('información chida')),
      )*/

class ListaWidget extends StatefulWidget {
  const ListaWidget({Key? key}) : super(key: key);

  @override
  _ListaWidgetState createState() => _ListaWidgetState();
}

class _ListaWidgetState extends State<ListaWidget> {
  late List<String> _contenido;
  late TextStyle _estilito;

  _ListaWidgetState() {
    _contenido = ["a", "b", "c", "d", "e"];
    _estilito = const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("APLICACION CON LISTA")),
        body: _construyeLista());
  }

  Widget _construyeLista() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemCount: _contenido.length,
        itemBuilder: (context, i) {
          return _construyeRow(_contenido[i]);
        });
  }

  Widget _construyeRow(String valor) {
    return ListTile(
        title: Text(valor, style: _estilito),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const VistaRequest()));
        });
  }
}

class VistaDetalle extends StatelessWidget {
  const VistaDetalle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("VISTA DETALLE!")),
        body: const Center(child: Text("INFO IMPORTANTE")));
  }
}

class VistaRequest extends StatefulWidget {
  const VistaRequest({Key? key}) : super(key: key);

  @override
  _VistaRequestState createState() => _VistaRequestState();
}

class _VistaRequestState extends State<VistaRequest> {
  late Future<List<Carro>> carros;

  @override
  void initState() {
    super.initState();
    carros = obtenerInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("REQUESTITA")),
      body: Center(
          child: FutureBuilder<List<Carro>>(
              future: carros,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Text(snapshot.data![0].modelo);
                } else if (snapshot.hasError) {
                  return const Text("error en snapshot");
                }

                return const CircularProgressIndicator();
              })),
    );
  }
}

// declarar clase a donde se mapee datos de json
class Carro {
  final String marca;
  final String modelo;
  final int anio;

  Carro({required this.marca, required this.modelo, required this.anio});

  factory Carro.fromJson(Map<String, dynamic> json) {
    return Carro(
        marca: json['marca'], modelo: json['modelo'], anio: json['anio']);
  }
}

// declarar funcion para hacer request
Future<List<Carro>> obtenerInfo() async {
  final response = await http.get(Uri.parse(
      'https://bitbucket.org/itesmguillermorivas/partial2/raw/45f22905941b70964102fce8caf882b51e988d23/carros.json'));

  if (response.statusCode == 200) {
    List<dynamic> list = jsonDecode(response.body);
    List<Carro> result = [];

    for (var actual in list) {
      Carro carroActual = Carro.fromJson(actual);
      result.add(carroActual);
    }

    print(result);
    return result;
  } else {
    throw Exception("ERROR EN REQUEST");
  }
}
